import socket

HOST = '127.0.0.1'
PORT = 8080
CHUNK_SIZE = 4096

class Server:
    @staticmethod
    def run():
        try:
            print(f"Starting server on {HOST}:{PORT}, press Ctrl+C to terminate...")
            while 1:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.bind((HOST, PORT))
                    s.listen()
                    conn, addr = s.accept()
                    with conn:
                        print('Connected by {}:{}'.format(*addr))
                        while True:
                            data = conn.recv(CHUNK_SIZE)
                            if not data:
                                print("Shutting connection with {}:{}".format(*addr))
                                break
                            print(data)
                            conn.sendall(data)
        except KeyboardInterrupt:
            print("/!\\ /!\\ Shutting down server! /!\\ /!\\")


if __name__ == "__main__":
    Server.run()
