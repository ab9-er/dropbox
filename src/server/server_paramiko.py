import paramiko
import socket
import time
import os
from paramiko.common import AUTH_FAILED, AUTH_SUCCESSFUL, OPEN_SUCCEEDED


HOST = 'localhost'
PORT = 8082
HOST_KEY = paramiko.RSAKey.from_private_key(open('id_rsa'))


class Server:
    def get_transport(conn):
        transport = paramiko.Transport(conn)
        transport.add_server_key(HOST_KEY)
        transport.set_subsystem_handler('sftp', 
                                        paramiko.SFTPServer,
                                        SFTPServerInterface)

        print("Starting transport server")
        transport.start_server(server=SFTPServer())
        print("Started")
        return transport

    def start_server():
        paramiko_level = getattr(paramiko.common, "DEBUG")
        paramiko.common.logging.basicConfig(level=paramiko_level)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        server_socket.bind((HOST, PORT))
        server_socket.listen(10)

        while True:
            conn, addr = server_socket.accept()
            transport = Server.get_transport(conn)
            channel = transport.accept()

            while transport.is_active():
                time.sleep(1)

class SFTPServerInterface(paramiko.SFTPServerInterface):
    ROOT = os.path.join("~")
    KEY = HOST_KEY

    def listdir(self, path):
        print("HELLO")


class SFTPServer(paramiko.ServerInterface):
    def check_auth_password(self, username, password):
        return AUTH_SUCCESSFUL

    def check_channel_request(self, kind, chanid):
        return OPEN_SUCCEEDED



if __name__ == "__main__":
    Server.start_server()
