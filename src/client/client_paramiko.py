import paramiko


HOST = "localhost"
PORT = 8082
USERNAME = "test"
PASSWORD = "securePassword1234!"
HOST_KEY = paramiko.RSAKey.from_private_key(open('id_rsa'))

 
def client():
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(HOST, PORT, username=USERNAME, password=PASSWORD)
    client.open_sftp()
    client.close()
