import socket

HOST = '127.0.0.1'
PORT = 8080


class TransferClient:
    @staticmethod
    def run(data):
        try:
            with socket.socket() as s:
                s.connect((HOST, PORT))
                s.sendall(data.encode())
        except ConnectionRefusedError:
            print(f"Server is not available at {HOST}:{PORT}")
            exit(1)
