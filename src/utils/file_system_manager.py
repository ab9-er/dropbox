import os
import hashlib
from src.client.client import TransferClient
import json


CHUNK_SIZE = 4096


class FileSystemManager:
    TARGET_DIR_STATE = {}

    @staticmethod
    def initialise_fs(dir_path):
        for dir_root, dir_name, files in os.walk(os.path.abspath(dir_path)):
            for _file in files:
                target_path = os.path.join(dir_root, _file)
                checksum_chunks = FileSystemManager.generate_checksum_chunks(
                    target_path)
                FileSystemManager.TARGET_DIR_STATE.update(
                    {os.path.relpath(os.path.join(dir_root, _file), start=dir_path): checksum_chunks})
        TransferClient.run(json.dumps(FileSystemManager.TARGET_DIR_STATE))
        return FileSystemManager.TARGET_DIR_STATE

    @staticmethod
    def generate_checksum_chunks(target_path):
        checksum_chunks = []
        try:
            with open(target_path, 'rb') as filereader:
                while 1:
                    chunk = filereader.read(CHUNK_SIZE)
                    if chunk:
                        checksum = hashlib.md5(chunk).hexdigest()
                        checksum_chunks.append(checksum)
                    else:
                        break
                return checksum_chunks
        except FileNotFoundError:
            return []

    @staticmethod
    def get_modifications(target_path):
        new_checksum = FileSystemManager.generate_checksum_chunks(target_path)
        old_checksum = FileSystemManager.TARGET_DIR_STATE.get(target_path)

        to_delete = list(set(old_checksum) - set(new_checksum)
                         ) if old_checksum else None
        to_add = list(set(new_checksum) - set(old_checksum)
                      ) if old_checksum else None

        return to_add, to_delete
