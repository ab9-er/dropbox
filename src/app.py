import time
import sys
from src.utils.file_system_manager import FileSystemManager
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from src.client.client import TransferClient
import os
import json


SYNC_DIR = "sync"

HOST = '127.0.0.1'
PORT = 8080
CHUNK_SIZE = 4096


class DirWatcher:
    def __init__(self, dir_path):
        self.observer = Observer()
        self.dir_path = dir_path

    def run(self):
        event_handler = ChangeHandler()
        self.observer.schedule(event_handler, self.dir_path, recursive=True)
        self.observer.start()

        try:
            print(f"Monitoring {self.dir_path}, press Ctrl+C to stop")
            while 1:
                time.sleep(5)
        except KeyboardInterrupt:
            print("/!\\ /!\\ STOPPING THE SYNC APP /!\\ /!\\")
            self.observer.stop()
            self.observer.join()


class ChangeHandler(FileSystemEventHandler):
    @staticmethod
    def on_created(event):
        print(f"Create event detected on {event.src_path}")
        rel_path = os.path.relpath(event.src_path, start=SYNC_DIR)
        checksum_object = {rel_path: FileSystemManager.generate_checksum_chunks(rel_path)}
        FileSystemManager.TARGET_DIR_STATE.update(checksum_object)
        TransferClient.run(json.dumps(FileSystemManager.TARGET_DIR_STATE))
        print(FileSystemManager.TARGET_DIR_STATE)

    @staticmethod
    def on_modified(event):
        if event.is_directory:
            return
        print(f"Modify event detected on {event.src_path}")
        rel_path = os.path.relpath(event.src_path, start=SYNC_DIR)
        modifications = FileSystemManager.get_modifications(rel_path)
        print(f"MODIFICATIONS: {modifications}")
        checksum_object = {rel_path: FileSystemManager.generate_checksum_chunks(rel_path)}
        FileSystemManager.TARGET_DIR_STATE.update(checksum_object)
        TransferClient.run(json.dumps(FileSystemManager.TARGET_DIR_STATE))
        print(FileSystemManager.TARGET_DIR_STATE)

    @staticmethod
    def on_moved(event):
        if event.is_directory:
            return
        print(f"Move event detected on {event.src_path}")
        rel_path = os.path.relpath(event.src_path, start=SYNC_DIR)
        FileSystemManager.TARGET_DIR_STATE.pop(rel_path)
        TransferClient.run(json.dumps(FileSystemManager.TARGET_DIR_STATE))
        print(FileSystemManager.TARGET_DIR_STATE)

    @staticmethod
    def on_deleted(event):
        print(f"Delete event detected on {event.src_path}")
        rel_path = os.path.relpath(event.src_path, start=SYNC_DIR)
        FileSystemManager.TARGET_DIR_STATE.pop(rel_path)
        TransferClient.run(json.dumps(FileSystemManager.TARGET_DIR_STATE))
        print(FileSystemManager.TARGET_DIR_STATE)


if __name__ == "__main__":
    try:
        SYNC_DIR = sys.argv[1]
        print(f"Watching {SYNC_DIR}")
    except IndexError:
        print("Watching sync")
    FileSystemManager.initialise_fs(SYNC_DIR)
    w = DirWatcher(SYNC_DIR)
    w.run()
