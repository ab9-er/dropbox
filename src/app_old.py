import os
import time


class DirMonitor:
    def __init__(self, dir_path) -> None:
        self.dir_path = dir_path
        self.dir_modified_time = os.stat(self.dir_path).st_mtime
        self.files = self._get_files(self.dir_path)
        self.touched = {"deleted": [], "modified": [], "added": []}

    def _get_files(self, dir_path):
        """Get files and their mtime

        Args:
            dir_path (str): directory path

        Returns:
            files(dict): filename and mtime dict object
        """
        files = {}
        with os.scandir(dir_path) as sf:
            for _file in sf:
                files.update({_file.name: _file.stat().st_mtime})
        return files

    def _get_mtime(self, filename):
        return os.stat(os.path.join(self.dir_path, filename)).st_mtime

    def monitor(self):
        """Monitor dir for modified time, if so, then check for files

        Args:
            None
        """
        dir_modified_time = os.stat(self.dir_path).st_mtime
        dir_files = self._get_files(self.dir_path)
        deleted = list(set(self.files) - set(dir_files))
        added = list(set(dir_files) - set(self.files))
        modified = list(filter(lambda _f: self._get_mtime(_f) != self.files.get(_f),
                               list(set(dir_files) - set(added))))

        if dir_modified_time != self.dir_modified_time:
            print(f"===============================")
            print(f"Dir {self.dir_path} is touched")
            print(f"deleted: {deleted}")
            print(f"added: {added}")
            self.touched["deleted"].extend(deleted)
            self.touched["added"].extend(added)
            self.files = dir_files
            self.dir_modified_time = dir_modified_time

        elif modified:
            print(f"===============================")
            print(f"File {modified} is modified")
            self.touched["modified"].extend(modified)
            self.files = dir_files


if __name__ == "__main__":
    dir_monitor = DirMonitor("tests/test2")
    try:
        print("Control + C to kill...\n")
        while 1:
            time.sleep(5)
            dir_monitor.monitor()
    except KeyboardInterrupt:
        print("/!\\ /!\\ Escape sequence pressed! /!\\ /!\\")
