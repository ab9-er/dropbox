clean:
	find . -name "*pycache*" | xargs rm -rf
	find . -name "*pytest_cache*" | xargs rm -rf

install:
	pip install -r requirements.txt

run-server:
	PYTHONPATH=${PWD} python src/server/server.py

run-client:
	PYTHONPATH=${PWD} python src/app.py ${DIR}
